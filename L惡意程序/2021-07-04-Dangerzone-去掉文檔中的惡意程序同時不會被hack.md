---
title: Dangerzone-Working-With-Suspicious-Documents-Without-Getting-Hacked
---


# Dangerzone: 去掉文檔中的惡意程序同時不會被hack  

**作者：Micah Lee**  
**譯者：atgfw**
**原文发布日期：2020-03-12**  
**原文：** https://tech.firstlook.media/dangerzone-working-with-suspicious-documents-without-getting-hacked

不知你是否曾經聽過這樣的信息安全建議：“不要打開附件”？這當然是個萬般不錯的建議，但很不幸，對於記者、社運人士和其他許多人都不可能去實踐這個建議。想像一下如果你是個記者，收到一封自稱是來自特朗普集團的人的郵件，帶著個附件寫著“特朗普退稅.pdf”。你覺得你真的會回覆那個人說，“對不起，我不打開附件的”，然後就不理他了？  

實際情況是，作為一個記者，你的工作就是打開來自陌生人的文檔，不管這些文檔是從郵件、從 Signal 或 WhatsApp，還是從 SecureDrop 而來。記者們還必須打開和閱讀從各式各樣的網站下載來的文檔，包括一些洩漏出來或被黑的電郵，或其他什麼潛在的無法信任的來源的文件。  

![](https://thenib.imgix.net/usq/616fb852-4518-4059-88e6-310a7b9b9540/631c49c0-518f-4106-816a-c4372129c5dc.png?auto=compress,format&cs=srgb&_=f270ff588f711d8e5eeb3b43ad0822a2)  

Dangerzone，一個新的由 [First Look Media](https://firstlook.media/) 開發的開源工具（發佈於 2020 印度 Goa 的 Nullcon 駭客大會），旨在解決這個問題。你可以把 Dangerzone 安裝在 Windows、Mac 或 Linux 電腦。它可以用於打開很多類型的文檔：PDF、微軟 Office、LibreOffice、圖像等。就算原始文檔很有毒能把你的電腦黑掉，Dangerzone 也能把文檔轉換成安全的 PDF 讓你安全地閱讀。  

你可以把這工具想像成把文檔打印出來然後再掃描進來，排除掉有害成份，只不過這一切都有這個工具完成。  


## 上手安裝

- 在 Mac 安裝 [.dmg](https://github.com/firstlookmedia/dangerzone/releases/latest)
    - Mac 用户还可以选择 Homebrew 的方式直接从命令行安装：  
    ```
      brew install --cask dangerzone
    ```
- 在 Windows 安裝 [.msi](https://github.com/firstlookmedia/dangerzone/releases/latest)
- 看看 Linux 安裝的 [wiki](https://github.com/firstlookmedia/dangerzone/wiki/Installing-Dangerzone)


## 一份文檔如何變得危險？

PDF 和 Office 其實都是難以置信的複雜。 它們可以被做成在打開文檔時自動從遠端的服務器加載圖片，並且跟蹤文檔在什麼時候和什麼地點（IP 地址）被打開。它們可以包含 Javascript 或 宏代碼，可以在打開時自動執行，然後控制你的整台機器（這也取決於你怎麼設置用於打開這些文檔的軟件的）。最後，就像其他所有的軟件一樣，這些你用於打開文檔的軟件 —— Preview、Adobe Reader、Microsoft Word, LibreOffice 等等 —— 會有 bug，而有時一些 bug 會被利用來控制你的電腦（你可以通過總是安裝最新的軟件更新來降低被黑的風險，如果這些軟件的開發者即時發現了 bug 並很快修復了的話）。

舉個例子，如果一個攻擊者知道了 Microsoft Word 的一個安全 bug（漏洞），他們可以精心地製作一個這樣的文檔，當用戶用了帶相關漏洞的 Word 打開之後，文檔裡的惡意代碼就會跑出來，暗中控制你的計算機了。通常他們要這麼做只需要引誘你打開這個文檔，也許，就是給你發一封迷惑性很強的釣魚郵件。  

一個真實的案例就是俄羅斯軍方情報部門做的 2016 美國大選的釣魚郵件和惡意文檔。首先，他們黑掉了一個叫 VR Systems 的美國大選供應商，並且得到了他們的客戶名單。然後他們給這些客戶（也就是包括搖擺州的選舉工作人員）發了122封郵件，用這個發件人郵箱：`vrelections@gmail.com`，帶上這樣的附件：`New EViD User Guides.docm`。  

![](https://thenib.imgix.net/usq/98df8001-beb9-4369-ae71-5661f7dbe1e5/10ffbae1-a9f6-4f0c-871e-203f3c26913e.jpeg?auto=compress,format&cs=srgb&_=e6e8925964cdb91a7ef776f8bef15301)


如果任何一個選舉工作人員用了帶漏洞的 Word 打開了這個郵件的附件，那麼裡面的惡意程序就會在電腦上建立一個後門，直接通向俄羅斯黑客那邊（我們不知道實際有沒有人打開過這個文檔，但很有可能）。  

如果你今天得到這個郵件並用 dangerzone 打開這個附件，它會被轉成一個安全的 PDF（`New EViD User Guides.docm`），你可以安全地用 PDF 查看器打開這個文檔，不用擔心被黑。  


## 受到 Qubes TrustedPDF 的啟發

（譯者注：此處涉及一個專業性較強的操作系統，略去；Dangerzone 會基於 Docker container 而非 Qubes 的 VM。）  


## dangerzone 如何工作

Dangerzone 使用 Linux 容器（用了兩個容器），也就是一種類似虛擬機但更快更輕量的隔離環境（跟宿主機共享一個 Linux 內核）。在 Mac 和 Windows 上運行容器最簡單的方法就是使用 [Docker Desktop](https://www.docker.com/products/docker-desktop)。所以如果你還沒有在你的電腦安裝這玩意，dangerzone在安裝時會幫你下載和安裝它。  

當 dangerzone 開啟容器的時候，它會禁用網路，並只掛載你想要轉換的、有嫌疑的文檔。所以如果一個惡意文檔想黑你的系統，它只能在你的容器內存活，並不能進入到你的日常系統，也用不了網路，所以它也沒什麼能做的。 

我們來看看 dangerzone 都具體做了什麼。啟動第一個容器：  
- 把一个轴卷（里面放了原始文檔）掛載到容器裡；（译注：所谓轴卷，当作是磁盘上的一块区域或文件夹好了）  
- 使用 LibreOffice 或 GraphicsMagick 來轉換原始文檔到 PDF；  
- 使用 poppler 把 PDF 切割成一页一页的，并把每一页都转成 PNG 格式； 
- 使用 GraphicsMagick 把 PNG 转成 RGB 像素数据；  
- 把 RGB 像素数据存到另一个轴卷里（不是刚才挂载的）  

然后第一个容器就退出了，启动第二个容器：  
- 挂载那个刚才放了 RGB 数据的轴卷；  
- 如果启用了 OCR， 使用 GraphicsMagick 把 RGB 像素数据转回 PNG 图片，再用 Tesseract 把 PNG 转成可以搜索的 PDF 文档；  
- 使用 poppler 把多个单页的 PDF 合成为一整个 PDF；  
- 使用 ghostscript 对最后的 PDF 进行瘦身；  
- 最后把这个安全的 PDF 存到另一个轴卷。  

然后这个容器也退出，用户就能至今打开刚才生成的安全 PDF 了。  

以下是 dangerzone 能转换到安全 PDF 的文档格式：

- PDF (.pdf)
- Microsoft Word (.docx, .doc)
- Microsoft Excel (.xlsx, .xls)
- Microsoft PowerPoint (.pptx, .ppt)
- ODF Text (.odt)
- ODF Spreadsheet (.ods)
- ODF Presentation (.odp)
- ODF Graphics (.odg)
- Jpeg (.jpg, .jpeg)
- GIF (.gif)
- PNG (.png)
- TIFF (.tif, .tiff)

## 但仍有可能被 hack 哦

就像所有的软件一样，dangerzone（更重要的是，它所依赖的那些软件如 LibreOffice 和 Docker）可能有安全 bug。恶意的文档常常设计了能攻击特定的软件的代码，如，mac 上的 Adobe Reader。也有可能有人会专门针对 dangerzone 制作一个恶意文档。攻击者需要能够把这些可能的漏洞都成功利用上，才能成功黑掉 dangerzone：  

- 利用到了 LibreOffice or GraphicsMagic 里的漏洞；  
- Linux 内核里的容器逃逸漏洞； 
- Mac 和 Windows 里的 Docker Desktop 虚拟机逃逸漏洞的利用。  

如果你刚好用 dangerzone 打开了这么一个恶意文档，程序就会启动第一个容器并开始转换过程。 当它用 LibreOffice 转换原始文档（比如，一个 docx）到 PDF， 恶意代码就会利用 Libreoffice 的漏洞来黑进这个容器里。然后它还得利用 Linux 内核的漏洞来逃出容器（以及所在的虚拟机），从而得以操纵整个计算机。  

如果你保持了 Docker Desktop 更新到最新，这样的攻击对于大多攻击者来说都是成本极高的。  


## Dangerzone 是开源的

这个工具还在早期开发阶段（成文时间：2020-03-12），还可能会有 bug。如果你发现了，请查阅 Github 上的 [issues](https://github.com/firstlookmedia/dangerzone/issues)，如果你要提的问题不存在就开一个。  

你可以在这里找到 Mac、Windows 和 Linux 版图形界面应用的代码：https://github.com/firstlookmedia/dangerzone  

你还可以在这里找到 Linux 容器的代码：https://github.com/firstlookmedia/dangerzone-converter

Dangerzone 以 MIT 许可证发布。  











